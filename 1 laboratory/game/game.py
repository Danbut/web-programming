questions = {"Сколько км составляет диаметр Земли? (округлить до тысяч) ": "13000",
             "Сколько лет составляют 1 миллиард секунд? (округлить до целых) ": "32",
             "Сколько миллиардов нейронов в мозге человека? ": "100",
             "Сколько цифр в числе соответствующему количеству возможных позиций в шахматах? ": "42",
             "Сколько цифр в числе соответствующему количеству атомов во вселенной? ": "77"}
score = 0


def game():
    info()
    print("Вопросы про числа: ")
    ask_questions()
    print("Сумма набранных баллов " + str(score))


def info():
    file = open("C:/Users/Daniel/Desktop/study/university/3-course/web programming/1 laboratory/game/info.txt")
    print(file.read())


def check_answer(answer, question):
    global score
    keep_asking = True
    game_over = False
    attempt = 0
    while keep_asking and attempt < 3:
        if answer == questions[question]:
            print("Верный ответ ")
            score += 1
            keep_asking = False
        else:
            if attempt < 2:
                answer = input("Неверный ответ. Попробуйте ещё раз ")
            attempt += 1
    if attempt == 3:
        print("Правильный ответ: " + questions[question])
        game_over = True
    return game_over


def ask_questions():
    for key in questions:
        answer = input(key)
        game_over = check_answer(answer, key)
        if game_over:
            return 0


game()


