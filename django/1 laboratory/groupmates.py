#coding:utf-8

import numpy

groupmates = [
    {
        "name": u"Иван",
        "group": "СИБ-701-О",
        "age": 20,
        "marks": [5, 5, 3, 4, 5]
    },
    {
        "name": u"Никита",
        "group": "СМБ-701-О",
        "age": 20,
        "marks": [5, 3, 4, 4, 5]
    },
    {
        "name": u"Михаил",
        "group": "СПБ-701-О",
        "age": 20,
        "marks": [5, 5, 4, 5, 5]
    },
    {
        "name": u"Глеб",
        "group": "СББ-701-О",
        "age": 20,
        "marks": [5, 4, 3, 3, 4]
    },
    {
        "name": u"Александр",
        "group": "СИБ-701-О",
        "age": 20,
        "marks": [5, 5, 5, 4, 4]
    }
]


def print_students(students):
    print(u"Имя студента".ljust(15),
          u"Группа".ljust(15),
          u"Возраст".ljust(8),
          u"Оценки".ljust(20))
    for student in students:
        print(student["name"].ljust(15),
              student["group"].ljust(15),
              str(student["age"]).ljust(8),
              str(student["marks"]).ljust(20))
    print("\n")


def students_filter(students):
    return sorted(students, key=lambda student: numpy.mean(student["marks"]), reverse=True)


print_students(students_filter(groupmates))
