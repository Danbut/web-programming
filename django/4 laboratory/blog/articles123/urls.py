from django.urls import path
from django.conf.urls import url

from . import views
from .views import get_article

urlpatterns = [
    path('', views.archive),
    path('<int:article_id>/',
        get_article,
        name='get_article'
    )
]